# 仓库简介<a name="ZH-CN_TOPIC_0000001431562625"></a>

在全面云化时代，数据是企业数字化转型的基石，华为云GaussDB，支持多种关系型数据库和非关系型数据库，能够为企业提供更安全、更稳定、更智能的云上数据库服务，包括数据复制服务DRS，数据管理服务DAS,数据库和应用迁移UGO，分布式数据中间件DDM。

# 项目总览<a name="ZH-CN_TOPIC_0000001380803808"></a>

<a name="table147871518185316"></a>
<table><thead align="left"><tr id="row378817182539"><th class="cellrowborder" valign="top" width="33.3033303330333%" id="mcps1.1.4.1.1"><p id="p5788818145318"><a name="p5788818145318"></a><a name="p5788818145318"></a>项目</p>
</th>
<th class="cellrowborder" valign="top" width="33.33333333333333%" id="mcps1.1.4.1.2"><p id="p2788131865311"><a name="p2788131865311"></a><a name="p2788131865311"></a>简介</p>
</th>
<th class="cellrowborder" valign="top" width="33.36333633363336%" id="mcps1.1.4.1.3"><p id="p167881118115318"><a name="p167881118115318"></a><a name="p167881118115318"></a>仓库</p>
</th>
</tr>
</thead>
<tbody><tr id="row778831875318"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p378871885318"><a name="p378871885318"></a><a name="p378871885318"></a>SQL语句转换</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p2078818182537"><a name="p2078818182537"></a><a name="p2078818182537"></a>实现异构数据库之间的SQL语句转换</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p17882189535"><a name="p17882189535"></a><a name="p17882189535"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-ugo-sql-conversion-java" target="_blank" rel="noopener noreferrer">huaweicloud-ugo-sql-conversion-java</a></p>
</td>
</tr>
<tr id="row978871855316"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p378821816539"><a name="p378821816539"></a><a name="p378821816539"></a>转换并迁移SQL</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p978871812536"><a name="p978871812536"></a><a name="p978871812536"></a>将源数据库SQL转换为目标数据库兼容的SQL并迁移</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p1788818105318"><a name="p1788818105318"></a><a name="p1788818105318"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-ugo-migration-project-java" target="_blank" rel="noopener noreferrer">huaweicloud-ugo-migration-project-java</a></p>
</td>
</tr>
<tr id="row27888186534"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p878831835313"><a name="p878831835313"></a><a name="p878831835313"></a>评估数据库</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p77881183536"><a name="p77881183536"></a><a name="p77881183536"></a>分析评估并选择目标数据库</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p578871813534"><a name="p578871813534"></a><a name="p578871813534"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-ugo-evaluation-project-java" target="_blank" rel="noopener noreferrer">huaweicloud-ugo-evaluation-project-java</a></p>
</td>
</tr>
<tr id="row5789418155315"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p0604192031211"><a name="p0604192031211"></a><a name="p0604192031211"></a>SQL加速方案</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p116042020121219"><a name="p116042020121219"></a><a name="p116042020121219"></a>基于云搜索服务的SQL加速</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p560472091212"><a name="p560472091212"></a><a name="p560472091212"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-search-acceleration-based-css" target="_blank" rel="noopener noreferrer">huaweicloud-solution-search-acceleration-based-CSS</a></p>
</td>
</tr>
<tr id="row10789141835317"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p1978921814530"><a name="p1978921814530"></a><a name="p1978921814530"></a>云上搭建高可用网站</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p578951814536"><a name="p578951814536"></a><a name="p578951814536"></a>通过云服务构建一个高可用网站</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p147891418135319"><a name="p147891418135319"></a><a name="p147891418135319"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-high-availability-website-architecture" target="_blank" rel="noopener noreferrer">huaweicloud-solution-high-availability-website-architecture</a></p>
</td>
</tr>
<tr id="row27894181533"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p117895186530"><a name="p117895186530"></a><a name="p117895186530"></a>游戏批量开服</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p1778941885318"><a name="p1778941885318"></a><a name="p1778941885318"></a>提高新游戏上线期间客户开服效率</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p4789171814539"><a name="p4789171814539"></a><a name="p4789171814539"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-game-rovision-database" target="_blank" rel="noopener noreferrer">huaweicloud-solution-game-rovision-database</a></p>
</td>
</tr>
<tr id="row999015196"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p2991415691"><a name="p2991415691"></a><a name="p2991415691"></a>暂停迁移</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p79991515919"><a name="p79991515919"></a><a name="p79991515919"></a>迁移中对任务进行暂停/续传</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p119941515913"><a name="p119941515913"></a><a name="p119941515913"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-show-job-list-java" target="_blank" rel="noopener noreferrer">huaweicloud-showJobList-Java</a></p>
</td>
</tr>
<tr id="row1365810151491"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p116588152091"><a name="p116588152091"></a><a name="p116588152091"></a>Redis使用场景</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p36582015598"><a name="p36582015598"></a><a name="p36582015598"></a>Redis配合其他组件使用</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p13658191516915"><a name="p13658191516915"></a><a name="p13658191516915"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-redis-sdk-samples" target="_blank" rel="noopener noreferrer">huaweicloud-redis-sdk-samples</a></p>
</td>
</tr>
<tr id="row1276818151596"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p11769615996"><a name="p11769615996"></a><a name="p11769615996"></a>数据库用户管理</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p12769215196"><a name="p12769215196"></a><a name="p12769215196"></a>通过java版本的SDK方式查看数据库用户列表，创建数据库用户。</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p1676913159914"><a name="p1676913159914"></a><a name="p1676913159914"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-gaussdbformysql-databaseuser-java" target="_blank" rel="noopener noreferrer">huaweicloud-gaussdbformysql-databaseuser-java</a></p>
</td>
</tr>
<tr id="row1786210151796"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p58631015797"><a name="p58631015797"></a><a name="p58631015797"></a>数据库用户管理</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p68631815499"><a name="p68631815499"></a><a name="p68631815499"></a>通过java版本的SDK方式对数据库用户进行授权</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p48631151192"><a name="p48631151192"></a><a name="p48631151192"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-gaussdbformysql-database-java" target="_blank" rel="noopener noreferrer">huaweicloud-gaussdbformysql-database-java</a></p>
</td>
</tr>
<tr id="row99553151091"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p17955181518915"><a name="p17955181518915"></a><a name="p17955181518915"></a>备份策略管理</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p99551915493"><a name="p99551915493"></a><a name="p99551915493"></a>通过java版本的SDK方式查看备份策略，修改备份策略。</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p7956101519910"><a name="p7956101519910"></a><a name="p7956101519910"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-gaussdbformysql-backup-java" target="_blank" rel="noopener noreferrer">huaweicloud-gaussdbformysql-backup-java</a></p>
</td>
</tr>
<tr id="row1762117161194"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p10606172016127"><a name="p10606172016127"></a><a name="p10606172016127"></a>重启集群</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p1660614206123"><a name="p1660614206123"></a><a name="p1660614206123"></a>重启集群时，利用华为云Java SDK确保业务能正常使用</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p1860613203125"><a name="p1860613203125"></a><a name="p1860613203125"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-css-rollingrestart-java" target="_blank" rel="noopener noreferrer">huaweicloud-css-rollingrestart-java</a></p>
</td>
</tr>
<tr id="row260116911169"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p6606122091219"><a name="p6606122091219"></a><a name="p6606122091219"></a>备份集群</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p1660692051218"><a name="p1660692051218"></a><a name="p1660692051218"></a>基于华为云Java SDK为用户创建集群快照，并对集群进行备份。</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p17606202010121"><a name="p17606202010121"></a><a name="p17606202010121"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-css-createsnapshot-java" target="_blank" rel="noopener noreferrer">huaweicloud-css-createsnapshot-java</a></p>
</td>
</tr>
<tr id="row365121111167"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p1960616207122"><a name="p1960616207122"></a><a name="p1960616207122"></a>分布式搜索引擎</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p17606192011122"><a name="p17606192011122"></a><a name="p17606192011122"></a>基于华为云Java SDK为用户创建基础依赖集群</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p1360662019121"><a name="p1360662019121"></a><a name="p1360662019121"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-css-createcluster-java" target="_blank" rel="noopener noreferrer">huaweicloud-css-createcluster-java</a></p>
</td>
</tr>
<tr id="row114973487295"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p1960618204126"><a name="p1960618204126"></a><a name="p1960618204126"></a>数据对比</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p46061202124"><a name="p46061202124"></a><a name="p46061202124"></a>通过Java版SDK创建对比任务并查询对比结果</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p86062020141214"><a name="p86062020141214"></a><a name="p86062020141214"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-batch-start-jobs-java" target="_blank" rel="noopener noreferrer">huaweicloud-batchStartJobs-Java</a></p>
</td>
</tr>
<tr id="row10604184812912"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p18607720111210"><a name="p18607720111210"></a><a name="p18607720111210"></a>MySQL迁移入云</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p76077205123"><a name="p76077205123"></a><a name="p76077205123"></a>通过数据复制服务SDK创建MySQL迁移入云任务</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p12607192016125"><a name="p12607192016125"></a><a name="p12607192016125"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-batch-create-jobs-java" target="_blank" rel="noopener noreferrer">huaweicloud-batchCreateJobs-Java</a></p>
</td>
</tr>
<tr id="row7699248102913"><td class="cellrowborder" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p7607162014120"><a name="p7607162014120"></a><a name="p7607162014120"></a>集成OBS和RDS</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p106079208126"><a name="p106079208126"></a><a name="p106079208126"></a>实现用户登录、图片上传、更新等功能，并在云端部署日志采集监控等内容。</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p5607132017120"><a name="p5607132017120"></a><a name="p5607132017120"></a><a href="https://gitee.com/HuaweiCloudDeveloper/dtse-practice" target="_blank" rel="noopener noreferrer">dtse-practice</a></p>
</td>
</tr>
<tr id="row2079310487298"><td class="cellrowborder" rowspan="3" valign="top" width="33.3033303330333%" headers="mcps1.1.4.1.1 "><p id="p1479434810293"><a name="p1479434810293"></a><a name="p1479434810293"></a>DDS API调用</p>
</td>
<td class="cellrowborder" valign="top" width="33.33333333333333%" headers="mcps1.1.4.1.2 "><p id="p117941548182917"><a name="p117941548182917"></a><a name="p117941548182917"></a>查看和扩容副本集实例存储空间</p>
</td>
<td class="cellrowborder" valign="top" width="33.36333633363336%" headers="mcps1.1.4.1.3 "><p id="p18794154852914"><a name="p18794154852914"></a><a name="p18794154852914"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dds-replset-storage-java" target="_blank" rel="noopener noreferrer">huaweicloud-dds-replset-storage-java</a></p>
</td>
</tr>
<tr id="row208881748142919"><td class="cellrowborder" valign="top" headers="mcps1.1.4.1.1 "><p id="p7888144832912"><a name="p7888144832912"></a><a name="p7888144832912"></a>查看指定实例的参数信息和修改需要重启生效的参数。</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.1.4.1.2 "><p id="p488834812911"><a name="p488834812911"></a><a name="p488834812911"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dds-params-config-java" target="_blank" rel="noopener noreferrer">huaweicloud-dds-params-config-java</a></p>
</td>
</tr>
<tr id="row29973482299"><td class="cellrowborder" valign="top" headers="mcps1.1.4.1.1 "><p id="p4998848122915"><a name="p4998848122915"></a><a name="p4998848122915"></a>查询并设置集群均衡开关和集群均衡活动时间窗</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.1.4.1.2 "><p id="p29981048132916"><a name="p29981048132916"></a><a name="p29981048132916"></a><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-dds-cluster-config-java" target="_blank" rel="noopener noreferrer">huaweicloud-dds-cluster-config-java</a></p>
</td>
</tr>
</tbody>
</table>

